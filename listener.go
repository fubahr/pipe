package pipe

import (
	"context"
	"net"
	"sync"
)

// Listener implements net.Listener and also provides several convenient functions for instantiating
// client connections to be serviced by the listener.
type Listener interface {
	net.Listener
	NewConn() (net.Conn, error)
	DialContextGRPC(context.Context, string) (net.Conn, error)
	DialContextHTTP(context.Context, string, string) (net.Conn, error)
}

// Listen instantiates and returns a new Listener.
func Listen() Listener {
	return &listener{
		mutex:         &sync.RWMutex{},
		connChan:      make(chan net.Conn, 16),
		closed:        false,
		serverAddress: newAddr("pipe"),
	}
}

type listener struct {
	mutex         *sync.RWMutex
	connChan      chan net.Conn
	closed        bool
	serverAddress addr
}

// CLose will close the listener so it cannot service any new connections. Existing connections will
// continue to function normally. If the listener is already closed, returns net.ErrClosed.
func (l *listener) Close() error {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	if l.closed {
		return net.ErrClosed
	}
	close(l.connChan)
	l.closed = true
	return nil
}

// Addr returns the address associated with the listener, always a randomly-generated UUID.
func (l *listener) Addr() net.Addr {
	return l.serverAddress
}
