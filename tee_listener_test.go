package pipe

import (
	"encoding/json"
	"fmt"
	"io"
	"net"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTeeListener(t *testing.T) {
	l1 := Listen().(*listener)
	l2 := Listen().(*listener)

	tl := TeeListener(l1, l2)
	defer tl.Close()

	type message struct {
		Name     string `json:"name"`
		Greeting string `json:"greeting"`
	}

	connCount := 0
	routineStopped := make(chan struct{})
	go func() {
		defer close(routineStopped)
		for {
			conn, err := tl.Accept()
			if err == net.ErrClosed {
				return
			}
			if !assert.NoError(t, err) || !assert.NotNil(t, conn) {
				return
			}
			connCount++

			msg := &message{}
			json.NewDecoder(conn).Decode(msg)
			msg.Greeting = fmt.Sprintf("hello, %s", msg.Name)
			json.NewEncoder(conn).Encode(msg)

			conn.Close()
		}
	}()

	testConn := func(l *listener, name string) {
		conn, err := l1.NewConn()
		if !assert.NoError(t, err) || !assert.NotNil(t, conn) {
			return
		}

		// send the designated name
		msg := &message{Name: name}
		err = json.NewEncoder(conn).Encode(msg)
		if !assert.NoError(t, err) {
			return
		}

		// validate the response is as expected
		err = json.NewDecoder(conn).Decode(msg)
		if !assert.NoError(t, err) {
			return
		}
		expectedMessage := fmt.Sprintf("hello, %s", name)
		assert.Equal(t, expectedMessage, msg.Greeting)

		// connection should have been closed by the server
		_, err = conn.Read(nil)
		assert.Equal(t, io.EOF, err)
	}

	testConn(l1, "foo")
	testConn(l2, "bar")

	// the listener should have handled exactly 2 incoming connections
	assert.Equal(t, 2, connCount)

	tl.Close() // closing the teelistener
	// should close all subordinate listeners
	assert.True(t, l1.closed)
	assert.True(t, l2.closed)

	select {
	case <-routineStopped:
	case <-time.After(10 * time.Second):
		t.Errorf("routine didn't stop 10 seconds after listener was closed")
	}
}
