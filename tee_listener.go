package pipe

import (
	"errors"
	"net"
	"sync"
)

// TeeListener returns a single net.Listener that aggregates connections from all
// desigated subordinate listeners.
func TeeListener(listeners ...net.Listener) net.Listener {
	numListeners := len(listeners)

	tl := &teeListener{
		address:          newAddr("tee"),
		listeners:        listeners,
		connErrorChan:    make(chan connError, numListeners*16),
		routineWaitGroup: &sync.WaitGroup{},
		closeMutex:       &sync.Mutex{},
	}

	tl.routineWaitGroup.Add(numListeners)
	for _, l := range tl.listeners {
		go tl.acceptRoutine(l)
	}

	return tl
}

type connError struct {
	conn net.Conn
	err  error
}

type teeListener struct {
	address          net.Addr
	listeners        []net.Listener
	connErrorChan    chan connError
	routineWaitGroup *sync.WaitGroup
	closeMutex       *sync.Mutex
	closed           bool
}

func (tl *teeListener) acceptRoutine(l net.Listener) {
	defer tl.routineWaitGroup.Done()

	for {
		conn, err := l.Accept()
		if errors.Is(err, net.ErrClosed) {
			return // the listener is closed so end routine
		}
		tl.connErrorChan <- connError{conn, err}
	}
}

func (tl *teeListener) Accept() (net.Conn, error) {
	ce, ok := <-tl.connErrorChan
	if !ok {
		tl.Close()
		return nil, net.ErrClosed
	}
	return ce.conn, ce.err
}

func (tl *teeListener) Close() error {
	tl.closeMutex.Lock()
	defer tl.closeMutex.Unlock()

	if tl.closed {
		return net.ErrClosed
	}

	var finalError error // capture only the first error encountered
	for _, l := range tl.listeners {
		if err := l.Close(); err != nil && finalError == nil {
			finalError = err
		}
	}

	tl.routineWaitGroup.Wait()

	close(tl.connErrorChan)
	tl.closed = true

	return finalError
}

func (tl *teeListener) Addr() net.Addr {
	return tl.address
}
