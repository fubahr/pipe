package pipe

import (
	"context"
	"net"
)

// NewConn instantiates a new client connection to the listener. Any services using the listener
// will be able to service the client connection once Accept returns it. Returns net.ErrClosed if
// the listener is closed.
func (l *listener) NewConn() (net.Conn, error) {
	l.mutex.RLock()
	defer l.mutex.RUnlock()
	if l.closed {
		return nil, net.ErrClosed
	}
	server, client := net.Pipe()
	clientAddress := newAddr("pipe")
	server = &conn{
		Conn:       server,
		localAddr:  l.serverAddress,
		remoteAddr: clientAddress,
	}
	client = &conn{
		Conn:       client,
		localAddr:  clientAddress,
		remoteAddr: l.serverAddress,
	}
	l.connChan <- server
	return client, nil
}

// DialContextGRPC is suitable for usage in google.golang.org/grpc.WithContextDialer
func (l *listener) DialContextGRPC(_ context.Context, _ string) (net.Conn, error) {
	return l.NewConn()
}

// DialContextHTTP is suitable for usage in net/http.Transport.DialContext
func (l *listener) DialContextHTTP(_ context.Context, _, _ string) (net.Conn, error) {
	return l.NewConn()
}

// Accept blocks until a connection is initiated by a client, and then returns it. If the listener
// is closed, then returns net.ErrClosed.
func (l *listener) Accept() (net.Conn, error) {
	conn, ok := <-l.connChan
	if !ok {
		return nil, net.ErrClosed
	}
	return conn, nil
}

type conn struct {
	net.Conn
	localAddr  addr
	remoteAddr addr
}

// LocalAddr returns the local address for the connection
func (c *conn) LocalAddr() net.Addr {
	return c.localAddr
}

// RemoteAddr returns the remote address for the connection
func (c *conn) RemoteAddr() net.Addr {
	return c.remoteAddr
}
