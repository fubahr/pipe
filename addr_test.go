package pipe

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestAddr(t *testing.T) {
	network := "foo"
	a := newAddr(network)
	u, err := uuid.Parse(a.String())
	if !assert.NoError(t, err) {
		return
	}

	assert.NotZero(t, u, "generated UUID is 0-value")
	assert.Equal(t, network, a.Network())
	assert.Equal(t, u.String(), a.String())
}
