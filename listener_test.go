package pipe

import (
	"bufio"
	"io"
	"math/rand"
	"net"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
)

func TestInterfaces(t *testing.T) {
	l := Listen()
	defer l.Close()

	// test usage of DialContextHTTP in http.Client
	_ = &http.Client{
		Transport: &http.Transport{
			DialContext: l.DialContextHTTP,
		},
	}

	// test usage of DialContextGRPC in grpc.WithContextDialer
	_ = grpc.WithContextDialer(l.DialContextGRPC)
}

func TestNewListener(t *testing.T) {
	l := Listen().(*listener)
	defer l.Close()
	assert.NotNil(t, l.mutex)
	assert.NotNil(t, l.connChan)
	assert.False(t, l.closed)
	assert.NotZero(t, l.serverAddress)
	assert.Equal(t, l.serverAddress, l.Addr())
}

func TestListenerClose(t *testing.T) {
	l := Listen()
	defer l.Close()

	var (
		c1, c2, c3 net.Conn
		err        error
	)

	c1, err = l.NewConn()
	assert.NoError(t, err)
	assert.NotNil(t, c1)
	defer c1.Close()

	c2, err = l.NewConn()
	assert.NoError(t, err)
	assert.NotNil(t, c2)
	defer c2.Close()

	l.Close() // closing listener

	c3, err = l.NewConn() // should return net.ErrClosed and nil connection
	assert.Equal(t, net.ErrClosed, err)
	assert.Nil(t, c3)

	// first two Accept calls should return connections
	_, err = l.Accept()
	assert.NoError(t, err)
	_, err = l.Accept()
	assert.NoError(t, err)

	// last one should show the internal connection channel exhausted, and
	// return net.ErrClosed
	_, err = l.Accept()
	assert.Equal(t, net.ErrClosed, err)
}

func TestListenerPipe(t *testing.T) {
	l := Listen()
	defer l.Close()

	testConns := func(conn1, conn2 net.Conn) {
		numBytes := 16

		// assert connections are non-nil
		assert.NotNil(t, conn1)
		assert.NotNil(t, conn1)

		// generate buffer with random junk data for sending
		sendBuffer := make([]byte, numBytes)
		rand.Read(sendBuffer)

		// send junk data. Must be buffered or write will block in absence
		// of a concurrent read.
		sender := bufio.NewWriterSize(conn1, numBytes)
		sender.Write(sendBuffer)
		go sender.Flush() // flush buffer in concurrent goroutine

		// receive data from sender
		receiveBuffer := make([]byte, numBytes)
		receiver := bufio.NewReaderSize(conn2, numBytes)
		io.ReadFull(receiver, receiveBuffer)

		// bytes sent and received should be equal
		assert.EqualValues(t, sendBuffer, receiveBuffer)
	}

	clientConn1, _ := l.NewConn()
	serverConn1, _ := l.Accept()
	clientConn2, _ := l.NewConn()
	serverConn2, _ := l.Accept()

	defer clientConn1.Close()
	defer serverConn1.Close()
	defer clientConn2.Close()
	defer serverConn2.Close()

	testConns(clientConn1, serverConn1)
	testConns(serverConn1, clientConn1) // full duplex
	testConns(clientConn2, serverConn2)
	testConns(serverConn2, clientConn2) // full duplex
}
