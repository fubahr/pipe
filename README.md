# Pipe

## what is this?
This is a simple package implementing a `net.Listener` that uses in-memory pipes instead of a network stack.

## why is this useful?
This package is only suitable for applications that must establish a connection with themselves. This use-cases are limited, but also very real. For example, you could use this package in reverse-proxy implementations like [GRPC Gateway](https://github.com/grpc-ecosystem/grpc-gateway).

## why not just use the network stack?
The network stack works in cases like this. However, it has numerous drawbacks, even when using the Loopback interface:
- Unnecessary architectural complexity
- Nonzero network latency
- Inferior reliability
- Dependency on OS system calls
- Avoidable attack surface
- Extraneous transport complexity and overhead like TLS and most notably certificate exchange

## Getting started

First of all, **use this module at your own risk**! While I personally use it, and I believe it works, it has never been deployed broadly in production contexts. **I take no responsibility for what this code does or does not do**.

### Installation
You can add this package to your current go module with the command `go get gitlab.com/fubahr/pipe`. Once you have done so, you can use the package freely within your module.

### Usage
You may instantiate a new pipe listener as follows:
```go
import "pipe"

listener := pipe.Listen()
```

The resulting `listener` implements `net.Listener` and can therefore be used in similar fashion:

```go
import "net/http"

http.HandleFunc("/hello", func(rw http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(rw, "Hello, world!")
})

http.Serve(listener)
```

The example HTTP server is listening only on the pipe listener, and cannot accept requests over a TCP socket. However, another routine in the same application can instantiate connections to `listener` by invoking `listener.NewConn` or specialty functions `listener.DialContextGRPC` or `listener.DialContextHTTP`. 

The last option, `listener.DialContextHTTP` can be used in an `http.Client` via the `http.Transport.DialContext` parameter:

```go
import "net/http"

client := &http.Client{
    Transport: &Transport{
        DialContext: listener.DialContextHTTP,
    },
    // be sure to include other important parameters
}

client.Get("/hello") // works!
```

### TeeListener

For convenience, the `TeeListener` can be used to wrap multiple `net.Listener`s into one. For example, you could instantiate a `PipeListener` and a more conventional TCP Listener, then wrap them together and pass them both to the same server instance. A few critical points:
* If the `TeeListener`.`Close` method is invoked, then all underlying listeners will be closed sequentially. If multiple listeners return an error when closed, only the first such error will be returned, but all listeners will be closed nonetheless.
* If any underlying listeners are closed independently, the overall `TeeListener` will continue functioning with any remaining listeners.

## Etc.

### Support
I hope this is useful but I can't offer any direct support. However, feel free to raise an issue and I will take a look eventually.

### Contributing
Feel free to open a merge request if you would like to improve this package. I'll take a look eventually.

### License
See [LICENSE](LICENSE)
