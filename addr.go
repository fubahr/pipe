package pipe

import (
	"github.com/google/uuid"
)

type addr struct {
	network, address string
}

func newAddr(network string) addr {
	return addr{
		network: network,
		address: uuid.NewString(),
	}
}

// Network returns the network associated with the pipe connection (always simply "pipe").
func (a addr) Network() string {
	return a.network
}

// String returns a string representation of the address, usually a randomly-generated UUID
func (a addr) String() string {
	return a.address
}
