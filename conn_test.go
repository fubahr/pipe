package pipe

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConn(t *testing.T) {
	localAddr := newAddr("foo")
	remoteAddr := newAddr("bar")
	c := &conn{
		localAddr:  localAddr,
		remoteAddr: remoteAddr,
	}

	assert.Equal(t, localAddr, c.LocalAddr())
	assert.Equal(t, remoteAddr, c.RemoteAddr())
}
